<?php
namespace Enova\Validation;

use Enova\Validation\Lang\En;
use Illuminate\Validation\Factory;
use Illuminate\Contracts\Container\Container;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Container\Container as IlluminateContainer;


class Validation
{
    /** @var Validation $INSTANCE */
    private static $INSTANCE = null;
    /** @var Factory $factory */
    protected $factory = null;

    private function __construct(Container $container = null)
    {
        $container = $container ? : new IlluminateContainer;

        $this->factory = new Factory($this->getTranslator($container), $container);
        if ($container && $container->bound('db')) {
            $this->setConnection($container['db']);
        }
    }

    public function getTranslator(Container $container )
    {
        if (!is_null($container) && $container->bound('translator')) {
             return $container['translator'];
        }

        return new Translator(new En());
    }

    public function getFactory()
    {}

    public static function getInstance(Container $container = null)
    {
        if(!static::$INSTANCE instanceof Validation){
            static::$INSTANCE = new static($container);
        }
        return static::$INSTANCE;
    }

    public function setConnection(ConnectionResolverInterface $connection)
    {}

    public function setLines(array $lines)
    {}

    public function __call($method, $arguments)
    {
        return call_user_func_array([
            static::getInstance()->getFactory(), $method], $arguments
        );
    }

    public static function __callStatic($method, $arguments)
    {
        return call_user_func_array([
            static::getInstance()->getFactory(), $method
        ], $arguments);
    }
}