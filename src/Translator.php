<?php
namespace Enova\Validation;

use ArrayObject;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;
use Enova\Validation\Lang\Interfaces\Language;
use Enova\Validation\Lang\Abtracts\Language as LanguageAbstract;


class Translator implements TranslatorInterface
{
    /** @var ArrayObject $lines */
    protected $lines = null;
    /** @var LanguageAbstract $lang */
    protected $lang = null;
    /** @var string $locale */
    protected $locale = null;

    public function __construct(Language $language)
    {
        $this->lang = $language;
    }

    public function getLocale()
    {}

    /**
     * Make the placeholder replacements in a line
     *
     * @param string $line
     * @param array $replace
     *
     * @return string
     */
    public function makeReplacements($line, array $replace)
    {
        $replace = (new Collection($replace))->sortBy(function($value, $key){
            return mb_strlen($key) * -1;
        });

        foreach ($replace as $key => $value){
            $line = str_replace(":{$key}", value, $line);
        }

        return $line;
    }

    /**
     * Set the lenguage lines used by the translator
     *
     * @param array $lines
     *
     * @return void
     */
    public function setLines(array $lines)
    {
        $this->lines->offsetSet('validation', $lines);
    }

    /**
     * Translates the given choice message by choosing a translation according to a number.
     *
     * @param string $id The message id (may also be an object that can be cast to string)
     * @param int $number The number to use to find the indice of the message
     * @param array $parameters An array of parameters for the message
     * @param string|null $domain The domain for the message or null to use the default
     * @param string|null $locale The locale or null to use the default
     *
     * @return string The translated string
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
        // TODO: Implement transChoice() method.
    }

    /**
     * Translates the given message.
     *
     * @param string $id The message id (may also be an object that can be cast to string)
     * @param array $parameters An array of parameters for the message
     * @param string|null $domain The domain for the message or null to use the default
     * @param string|null $locale The locale or null to use the default
     *
     * @return string The translated string
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        $line = Arr::get($this->lang->getConfig(), $id);

        if (is_null($line)){
            return $id;
        }else{
            return $this->makeReplacements($line, $parameters);
        }
    }

    /**
     * Sets the current locale.
     *
     * @param string $locale The locale
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
}