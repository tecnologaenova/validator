<?php
namespace Enova\Validation\Lang\Interfaces;


interface Language
{
    /**
     * @return array
     */
    public function getConfig();

    public function setLines(array $lines);
}