<?php
namespace Enova\Validation\Lang\Abtracts;

use ArrayObject;


abstract class Language implements \Enova\Validation\Lang\Interfaces\Language
{

    /** @var ArrayObject $config */
    private $config = null;

    protected function setConfig(ArrayObject $config){
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config->getArrayCopy();
    }
    public function setLines(array $lines)
    {
        $this->config->exchangeArray(array_merge($this->config->getArrayCopy()), $lines);
    }
}